package com.cognizant.rede.devtest;

import org.apache.log4j.Logger;

import com.itko.lisa.vse.stateful.model.Request;
import com.itko.util.ParameterList;
import com.itko.util.Parameter;

@SuppressWarnings("deprecation")
public class RequestDevTest{

	/**
	 * @author Fabricio Moreira
	 * @since 208-06-11
	 */
	
	Logger logger = Logger.getLogger(RequestDevTest.class);
	
	private Request request;
	private ParameterList pl;
	private String op;
	
	
	public RequestDevTest() {
	}


	public RequestDevTest(Request request) {
		setRequest(request);
		this.pl = request.getArguments();
		this.extrairOperacao();
		this.extrairParameterList();
	}


	public Request getRequest() {
		return request;
	}


	public void setRequest(Request request) {
		this.request = request;
	}


	public ParameterList getPl() {
		return pl;
	}


	public void setPl(ParameterList pl) {
		this.pl = pl;
		this.request.setArguments(pl);
		logger.info("Request: "+ this.request.toString());
	}
	
	public void plAddParameter(String nome, String valor) {
		this.pl.addParameter(new Parameter(nome, valor));
		this.setPl(pl);
	}
	
	public String plGetParameterValue(String nome) {
		String valor = this.pl.getParameterValue(nome);
		return valor;
	}


	public String getOp() {
		return op;
	}


	public void setOp(String op) {
		this.op = op;
		this.request.setOperation(op);
	}
	
	private void extrairOperacao() {
		op = request.getOperation();
	}
	
	private void extrairParameterList() {
		pl = request.getArguments();
	}
	
	
}
