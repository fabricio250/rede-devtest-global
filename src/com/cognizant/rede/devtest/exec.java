package com.cognizant.rede.devtest;

import com.itko.lisa.vse.stateful.model.Request;
import com.itko.util.Parameter;
import com.itko.util.ParameterList;

public class exec {

    public static void main(String[] args) {

        Request request = new Request();
        request.setOperation("0200");

        ParameterList pl = new ParameterList();

        pl.addParameter(new Parameter("2", "165448283487360007"));
        pl.addParameter(new Parameter("3", "003004"));
        pl.addParameter(new Parameter("4", "000000000100"));
        pl.addParameter(new Parameter("7", "0208165615"));
        pl.addParameter(new Parameter("11", "008561"));
        pl.addParameter(new Parameter("14", "1901"));
        pl.addParameter(new Parameter("18", "1337"));
        pl.addParameter(new Parameter("42", "012341088      "));
        pl.addParameter(new Parameter("43", "                       EDI           GBR"));
        pl.addParameter(new Parameter("48", "019 420701032109203073"));
        pl.addParameter(new Parameter("49", "986"));
        pl.addParameter(new Parameter("124", "02600003800900019715285000000"));

        request.setArguments(pl);


    RequestDevTest requestDevTest = new RequestDevTest(request);

        System.out.println(requestDevTest.getOp());
    }

}
